package com.example.msyo19.activiad10j;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    MyServicio myServicio;
    boolean myBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MyServicio.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (myBound) {
            unbindService(mConnection);
            myBound = false;
        }
    }

    public void parar(View v){
        if(myBound){
            unbindService((mConnection));
            myBound = false;
            Toast.makeText(this, "Servicio desconectado", Toast.LENGTH_SHORT).show();
        }
    }

    public void numero(View v) {
        if (myBound) {
            int num = myServicio.getRandomNumber();
            Toast.makeText(this, "Numero: " + num, Toast.LENGTH_SHORT).show();
        }
    }

    public void iniciar(View v){
        if(!myBound){
            Intent intent = new Intent(this, MyServicio.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            Toast.makeText(this, "Servicio iniciado", Toast.LENGTH_SHORT).show();
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            MyServicio.LocalBinder binder = (MyServicio.LocalBinder) service;
            myServicio = binder.getService();
            myBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            myBound = false;
        }
    };

}
