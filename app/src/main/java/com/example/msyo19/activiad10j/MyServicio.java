package com.example.msyo19.activiad10j;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;


import java.util.Random;

public class MyServicio extends Service {

    private final IBinder myb = new LocalBinder();
    private final Random mGenerator = new Random();

    public class LocalBinder extends Binder {
        MyServicio getService() {
            return MyServicio.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myb;
    }

    public int getRandomNumber() {
        return mGenerator.nextInt(500);
    }
}
